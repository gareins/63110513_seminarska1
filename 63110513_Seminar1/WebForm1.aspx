﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="_63110513_Seminar1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            color: #006600;
            font-size: large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="auto-style1">
    
        <strong>Oddelki in skupine izdelkov<br />
        <br />
        <asp:DropDownList ID="DDL_Categories" runat="server" AutoPostBack="True" BackColor="#66FF33" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="ProductCategoryID" Height="16px" Width="162px">
        </asp:DropDownList>
        <br />
        <br />
        </strong>
        <asp:GridView ID="GV_Categories" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductCategoryID" DataSourceID="SqlDataSource2" GridLines="None" OnSelectedIndexChanged="GV_Categories_SelectedIndexChanged" SelectedIndex="0" ShowHeader="False" style="font-size: medium; color: #000000" Width="158px">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="ProductCategoryID" HeaderText="ProductCategoryID" InsertVisible="False" ReadOnly="True" SortExpression="ProductCategoryID" Visible="False" />
                <asp:BoundField DataField="ParentProductCategoryID" HeaderText="ParentProductCategoryID" SortExpression="ParentProductCategoryID" Visible="False" />
                <asp:CommandField ButtonType="Button" SelectText="&gt;" ShowSelectButton="True" />
            </Columns>
            <SelectedRowStyle BackColor="#66FF33" BorderColor="#003300" />
        </asp:GridView>
        <hr />
        <strong>
        <br />
        Modeli<br />
        <br />
        </strong>
        <asp:GridView ID="GV_Models" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ProductModelID" DataSourceID="SqlDataSource3" GridLines="None" PageSize="5" SelectedIndex="0" ShowHeader="False" style="font-size: medium; color: #000000" Width="1154px">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                <asp:BoundField DataField="ProductCategoryID" HeaderText="ProductCategoryID" SortExpression="ProductCategoryID" Visible="False" />
                <asp:BoundField DataField="Culture" HeaderText="Culture" SortExpression="Culture" Visible="False" />
                <asp:BoundField DataField="ProductModelID" HeaderText="ProductModelID" ReadOnly="True" SortExpression="ProductModelID" Visible="False" />
                <asp:CommandField SelectText="Prikaži izdelke" ShowSelectButton="True" />
            </Columns>
            <RowStyle Width="10px" />
            <SelectedRowStyle BackColor="#66FF33" BorderColor="#003300" />
        </asp:GridView>
        <hr />
        <strong>
        <br />
        Izdelki<br />
        <br />
        </strong>
        <asp:GridView ID="GV_Products" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ProductID" DataSourceID="SqlDataSource4" GridLines="None" OnSelectedIndexChanged="GV_Products_SelectedIndexChanged" style="font-size: medium; color: #000000" Width="846px">
            <Columns>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra izdelka" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv izdelka" SortExpression="Name" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="ListPrice" DataFormatString="{0:c}" HeaderText="Cena" SortExpression="ListPrice" />
                <asp:BoundField DataField="ProductCategoryID" HeaderText="ProductCategoryID" SortExpression="ProductCategoryID" Visible="False" />
                <asp:BoundField DataField="ProductModelID" HeaderText="ProductModelID" SortExpression="ProductModelID" Visible="False" />
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID" Visible="False" />
                <asp:CommandField SelectText="Podrobnosti" ShowSelectButton="True" />
            </Columns>
            <HeaderStyle HorizontalAlign="Left" />
            <SelectedRowStyle BackColor="#66FF33" BorderColor="#003300" />
        </asp:GridView>
        <strong>
        <br />
        <asp:Label ID="L_Product" runat="server" style="font-size: medium" Text="Podatki o izdelku" Visible="False"></asp:Label>
        <br />
        <br />
        </strong>
        <asp:DetailsView ID="DV_Product" runat="server" AutoGenerateRows="False" DataKeyNames="ProductID" DataSourceID="SqlDataSource5" GridLines="None" Height="50px" style="font-size: medium; color: #000000" Width="628px">
            <Fields>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv" SortExpression="Name" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Weight" HeaderText="Teža" SortExpression="Weight" />
                <asp:BoundField DataField="StandardCost" DataFormatString="{0:c}" HeaderText="Nabavna cena" SortExpression="StandardCost" />
                <asp:BoundField DataField="ListPrice" DataFormatString="{0:c}" HeaderText="Prodajna cena" SortExpression="ListPrice" />
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID" Visible="False" />
            </Fields>
        </asp:DetailsView>
        <strong>
        <br />
        <asp:Label ID="L_Orders" runat="server" style="font-size: medium" Text="Seznam naročil" Visible="False"></asp:Label>
        <br />
        <br />
        </strong>
        <asp:GridView ID="GV_Orders" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource6" GridLines="None" style="color: #000000" Width="1045px">
            <Columns>
                <asp:BoundField DataField="SalesOrderID" HeaderText=" " SortExpression="SalesOrderID" />
                <asp:BoundField DataField="OrderDate" DataFormatString="{0:d}" HeaderText="Datum" SortExpression="OrderDate" />
                <asp:BoundField DataField="Kupec" HeaderText="Kupec" ReadOnly="True" SortExpression="Kupec" />
                <asp:BoundField DataField="OrderQty" HeaderText="Količina" SortExpression="OrderQty" />
                <asp:BoundField DataField="UnitPrice" DataFormatString="{0:c}" HeaderText="Cena enote" SortExpression="UnitPrice" />
                <asp:BoundField DataField="UnitPriceDiscount" DataFormatString="{0:p}" HeaderText="Popust" SortExpression="UnitPriceDiscount" />
                <asp:BoundField DataField="LineTotal" DataFormatString="{0:c}" HeaderText="Cena" ReadOnly="True" SortExpression="LineTotal" />
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID" Visible="False" />
            </Columns>
            <HeaderStyle Font-Underline="True" HorizontalAlign="Left" />
        </asp:GridView>
        <strong>
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Name, ProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID IS NULL)"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Name, ProductCategoryID, ParentProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDL_Categories" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT SalesLT.ProductModel.Name, SalesLT.ProductDescription.Description, SalesLT.Product.ProductCategoryID, SalesLT.ProductModelProductDescription.Culture, SalesLT.ProductModel.ProductModelID FROM SalesLT.Product INNER JOIN SalesLT.ProductModel ON SalesLT.Product.ProductModelID = SalesLT.ProductModel.ProductModelID INNER JOIN SalesLT.ProductModelProductDescription ON SalesLT.ProductModel.ProductModelID = SalesLT.ProductModelProductDescription.ProductModelID INNER JOIN SalesLT.ProductDescription ON SalesLT.ProductModelProductDescription.ProductDescriptionID = SalesLT.ProductDescription.ProductDescriptionID WHERE (SalesLT.Product.ProductCategoryID = @filter) AND (SalesLT.ProductModelProductDescription.Culture = 'en')">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductNumber, Name, Size, Color, ListPrice, ProductCategoryID, ProductModelID, ProductID FROM SalesLT.Product WHERE (ProductCategoryID = @filter) AND (ProductModelID = @filter2)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" Name="filter" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="GV_Models" Name="filter2" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductNumber, Name, Color, Size, Weight, StandardCost, ListPrice, ProductID FROM SalesLT.Product WHERE (ProductID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.SalesOrderDetail.SalesOrderID, SalesLT.SalesOrderHeader.OrderDate, SalesLT.Customer.CompanyName + ' (' + SalesLT.Address.City + ', ' + SalesLT.Address.CountryRegion + ')' AS Kupec, SalesLT.SalesOrderDetail.OrderQty, SalesLT.SalesOrderDetail.UnitPrice, SalesLT.SalesOrderDetail.UnitPriceDiscount, SalesLT.SalesOrderDetail.LineTotal, SalesLT.SalesOrderDetail.ProductID FROM SalesLT.SalesOrderDetail INNER JOIN SalesLT.SalesOrderHeader ON SalesLT.SalesOrderDetail.SalesOrderID = SalesLT.SalesOrderHeader.SalesOrderID INNER JOIN SalesLT.Customer ON SalesLT.SalesOrderHeader.CustomerID = SalesLT.Customer.CustomerID INNER JOIN SalesLT.Address ON SalesLT.SalesOrderHeader.ShipToAddressID = SalesLT.Address.AddressID AND SalesLT.SalesOrderHeader.BillToAddressID = SalesLT.Address.AddressID WHERE (SalesLT.SalesOrderDetail.ProductID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        </strong>
    
    </div>
    </form>
</body>
</html>
