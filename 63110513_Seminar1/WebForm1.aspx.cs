﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63110513_Seminar1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GV_Products_SelectedIndexChanged(object sender, EventArgs e)
        {
            L_Product.Visible = false;
            L_Orders.Visible = false;
            DV_Product.Visible = false;
            GV_Orders.Visible = false;

            if (GV_Products.SelectedIndex > -1)
            {
                L_Product.Visible = true;
                L_Orders.Visible = true;
                DV_Product.Visible = true;
                GV_Orders.Visible = true;
            }
        }

        protected void GV_Categories_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Models.SelectedIndex = 0;
            GV_Products.SelectedIndex = -1;
            
            DV_Product.Visible = false;
            GV_Orders.Visible = false;
            L_Product.Visible = false;
            L_Orders.Visible = false;
        }
    }
}